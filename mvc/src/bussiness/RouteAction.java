package bussiness;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.myservice.route.RouteService;
import service.myservice.route.RouteService2Impl;
import service.myservice.route.RouteServiceImpl;
import service.vo.MessageVO;
import struts.action.Action;
import struts.form.ActionForm;

public class RouteAction implements Action {

	
	public RouteAction() {
		// TODO Auto-generated constructor stub
	}

	public String execute(HttpServletRequest request,HttpServletResponse response,ActionForm form,Map<String,String> actionforward) {
		String url = "";
		RouteService service = new RouteService2Impl();
		Map<String, MessageVO> map = service.getVO(request.getParameter("text"));
		Set<String> set = map.keySet();
		for (Iterator<String> it = set.iterator(); it.hasNext();) {
			url = it.next();
			request.setAttribute("vo", map.get(url));
		}
		// session
		// HttpSession session=request.getSession();
		// session.setAttribute("x", name);
		// Application
		/*
		 * ServletContext application = this.getServletContext();
		 * application.setAttribute("x", name);
		 */
		// 获得了转发器对象,当前台请求过来的时候，request就直接失效了，这里是重新建立了一个request对象
		return url;
	}

}
