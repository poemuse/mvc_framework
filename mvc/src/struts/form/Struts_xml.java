package struts.form;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;

/**
 * 解析xml
 * 
 * @author David_www.yigongke.com_QQ_519501574
 *
 */
public class Struts_xml {

	public Struts_xml() {

	}

	public static Map<String, XmlBean> struts_xml(String xmlpath) throws Exception {
		SAXBuilder builder = new SAXBuilder();
		Document document = builder.build(new File(xmlpath));
		Element root = document.getRootElement();
		// Action也可能是多个
		Map<String, XmlBean> rmap = new HashMap<String, XmlBean>();
		Element actionroot = root.getChild("action-mapping");
		// Action可能有多个，一个action对应一个处理类
		List<Element> actions = actionroot.getChildren();
		for (Element e : actions) {
			XmlBean action = new XmlBean();
			String name = e.getAttributeValue("name");
			action.setBeanName(name);
			Element actionform = root.getChild("formbeans");
			// bean可能有多个，一个bean对应一个form
			List<Element> forms = actionform.getChildren();
			for (Element ex : forms) {
				if (name.equals(ex.getAttributeValue("name"))) {
					String formclass = ex.getAttributeValue("class");
					action.setFormClass(formclass);
					action.setActionClass(formclass);
					break;
				}
			}
			String path = e.getAttributeValue("path");
			action.setPath(path);
			String type = e.getAttributeValue("type");
			action.setActionType(type);
			List<Element> forward = e.getChildren();
			Map<String, String> map = new HashMap<String, String>();
			// 将转跳路径添加进去
			for (Element x : forward) {
				String fname = x.getAttributeValue("name");
				String fvalue = x.getAttributeValue("value");
				map.put(fname, fvalue);
			}
			action.setActionForward(map);
			rmap.put(path, action);
		}
		return rmap;
	}
}
