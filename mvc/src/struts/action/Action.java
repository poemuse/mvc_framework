package struts.action;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import struts.form.ActionForm;
/**
 * response 上传下载是需要用到的！！
 * @author David_www.yigongke.com_QQ_519501574
 *
 */
public interface Action {
	String execute(HttpServletRequest request, HttpServletResponse response, ActionForm form,
			Map<String, String> actionforward);
}
