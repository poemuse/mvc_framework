package service.myservice.route;

import java.util.Map;

import service.vo.MessageVO;

public interface RouteService {
	Map<String,MessageVO> getVO(String user);
}
